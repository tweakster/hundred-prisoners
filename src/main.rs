use std::cmp;

use clap::Parser;
use fastrand;

struct Room {
    size: usize,
    boxes: Vec<usize>
}

impl Room {
    pub fn new(size: usize) -> Self {
        let mut boxes: Vec<usize> = Vec::with_capacity(size);

        for i in 0..size {
            boxes.push(i);
        }

        fastrand::shuffle(&mut boxes);
    
        Self {
            size: size,
            boxes: boxes,
        }
    }

    // We have full control over num internally so not worrying about invalid values
    pub fn open_box(&self, num: usize) -> usize {
        if let Some(val) = self.boxes.get(num) {
            *val
        } else {
            usize::MAX
        }
    }
}

enum Strategy {
    Chain,
    Random,
}

fn attempt(strategy: &Strategy, attempts: usize, target: usize, room: &Room) -> Option<usize> {
    match strategy {
        Strategy::Chain => attempt_chain(attempts, target, room),
        Strategy::Random => attempt_random(attempts, target, room),
    }
}

fn attempt_chain(attempts: usize, target: usize, room: &Room) -> Option<usize> {

    let mut tries: usize = 0;
    let mut found: bool = false;
    let mut next_box: usize = target;

    loop {
        tries += 1;
        if tries > attempts {
            break;
        }

        let box_contents = room.open_box(next_box);

        if box_contents == target {
            found = true;
            break;
        }

        next_box = box_contents;
    }

    if found {
        Some(tries)
    } else {
        None
    }
}

fn attempt_random(attempts: usize, target: usize, room: &Room) -> Option<usize> {
    let mut choices: Vec<usize> = Vec::with_capacity(room.size);

    for i in 0..room.size {
        choices.push(i);
    }

    fastrand::shuffle(&mut choices);

    let mut tries: usize = 0;
    let mut found: bool = false;

    for c in choices.iter() {
        tries += 1;
        if tries > attempts {
            break;
        }

        if room.open_box(*c) == target {
            found = true;
            break;
        }
    }

    if found {
        Some(tries)
    } else {
        None
    }
}


fn analyse(num_prisoners: usize) {
    let room = Room::new(num_prisoners);
    let mut used: Vec<bool> = vec![false; room.size];
    let mut num_chains: usize = 0;
    let mut shortest_chain: usize = usize::MAX;
    let mut longest_chain: usize = 0;

    loop {
        // Find first unused
        let mut next_start: Option<usize> = None;

        for i in 0..used.len() {
            if ! used[i] {
                next_start = Some(i);
                break;
            }
        }
        let start_index = match next_start {
            None => break,
            Some(i) => i
        };

        // Find all the boxes in the chain
        used[start_index] = true;
        let mut next_index = room.open_box(start_index);
        let mut chain_length: usize = 1;

        while next_index != start_index {
            next_index = room.open_box(next_index);
            used[next_index] = true;
            chain_length += 1;
        }

        num_chains += 1;
        longest_chain = cmp::max(chain_length, longest_chain);
        shortest_chain = cmp::min(chain_length, shortest_chain);
    }

    println!("{} chains found", num_chains);
    println!("Longest: {}  Shortest: {}", longest_chain, shortest_chain);

}

fn run(num_prisoners: usize, strategy: &Strategy) {
    let attempts = num_prisoners / 2;

    let room = Room::new(num_prisoners);
    
    let mut escapees = 0;
    let mut initial_escapees = 0;
    let mut total_tries = 0;

    for i in 0..num_prisoners {
        let result = attempt(strategy, attempts, i, &room);
        if let Some(tries) = result {
            if initial_escapees == i {initial_escapees += 1;}
            escapees += 1;
            total_tries += tries;
        } else {
            total_tries += attempts;
        }
    }

    println!("Of {} prisoners {} escaped, {} before the first fail.",
        num_prisoners, escapees, initial_escapees);
    println!("Average number of attempts {}.", total_tries / num_prisoners);
}

#[derive(Parser)]
struct Args {
    #[arg(short, long)]
    analyse: bool,

    #[arg(short, long)]
    chain: bool,

    #[arg(short, long)]
    random: bool,

    #[arg(short, long, default_value_t = 100)]
    prisoners: usize
}

fn main() -> Result<(), &'static str> {
    let args = Args::parse();

    if args.analyse as usize + args.chain as usize + args.random as usize > 1 {
        return Err("--analyse --chain and --random are exclusive. Only select one")
    }

    if args.analyse {
        analyse(args.prisoners);
        return Ok(())
    }

    let strategy;
    if args.random {
        strategy = Strategy::Random;
    } else {
        strategy = Strategy::Chain;
    }

    run(args.prisoners, &strategy);
    Ok(())
}
